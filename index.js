var scraperjs = require('scraperjs');
var RequestPromise = require('request-promise');
scraperjs.StaticScraper.create('http://qi.com/feed')
  .scrape(function($) {
    return $('.description').map(function () {
      return $(this).text();
    }).get();
  })
  .then(function(fact) {
      const options = {
          uri: process.env.SLACK_HOOK,
          method: 'POST',
          json: true,
          body: {
              "attachments": [
                    {
                        "fallback": "@channel " + fact[0],
                        "color": "#1A76A7",
                        "title": "Stand Up Time! New & Improved Fact of the Day",
                        "text": "@channel " + fact[0],
                        "mrkdwn": "in_text"
                    }
                ],
               "username": "Jean Dan-Ali",
               "link_names": 1
          }
      }
      return RequestPromise(options).then((data) => {
          console.log("Posted Fact: " + fact[0]);
      });
  });
